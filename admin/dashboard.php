<?php include('include/admin/header.php'); ?>
<section>
		<div class="container">
			<div class="row">
                <?php include('include/admin/sidebar.php'); ?>
                <div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Dashboard</h2>
          </div>
					<script>
					(function(w,d,s,g,js,fs){
					  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
					  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
					  js.src='https://apis.google.com/js/platform.js';
					  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
					}(window,document,'script'));
					</script>
					<!-- Global site tag (gtag.js) - Google Analytics -->
					<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127451783-1"></script>
					<script>
					  window.dataLayer = window.dataLayer || [];
					  function gtag(){dataLayer.push(arguments);}
					  gtag('js', new Date());

					  gtag('config', 'UA-127451783-1');
					</script>

					<div id="embed-api-auth-container"></div>
					<div id="chart-container"></div>
					<div id="view-selector-container"></div>

					<script>

					gapi.analytics.ready(function() {

					  /**
					   * Authorize the user immediately if the user has already granted access.
					   * If no access has been created, render an authorize button inside the
					   * element with the ID "embed-api-auth-container".
					   */
					  gapi.analytics.auth.authorize({
					    container: 'embed-api-auth-container',
					    clientid: '264066071121-q3ilcouj2igp01de2mbbc8ag1aeiqots.apps.googleusercontent.com'

					  });


					  /**
					   * Create a new ViewSelector instance to be rendered inside of an
					   * element with the id "view-selector-container".
					   */
					  var viewSelector = new gapi.analytics.ViewSelector({
					    container: 'view-selector-container'
					  });

					  // Render the view selector to the page.
					  viewSelector.execute();


					  /**
					   * Create a new DataChart instance with the given query parameters
					   * and Google chart options. It will be rendered inside an element
					   * with the id "chart-container".
					   */
					  var dataChart = new gapi.analytics.googleCharts.DataChart({
					    query: {
					      metrics: 'ga:sessions',
					      dimensions: 'ga:date',
					      'start-date': '30daysAgo',
					      'end-date': 'yesterday'
					    },
					    chart: {
					      container: 'chart-container',
					      type: 'LINE',
					      options: {
					        width: '100%'
					      }
					    }
					  });


					  /**
					   * Render the dataChart on the page whenever a new view is selected.
					   */
					  viewSelector.on('change', function(ids) {
					    dataChart.set({query: {ids: ids}}).execute();
					  });

					});
					</script>

    </div>
    </div>
    </div>

    </section>


<?php include('include/admin/footer.php'); ?>
