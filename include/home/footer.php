<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 For Educational Purpose. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="#">Atem Holison-s3665602</a></span></p>
				</div>
			</div>
		</div>


    <script src="js/main.js"></script>
    <script src="js/script.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126562533-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-126562533-1');
		</script>

</body>
</html>
