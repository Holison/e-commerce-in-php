SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS shop DEFAULT CHARACTER SET utf8;
USE shop;

CREATE TABLE cart (
  ID int(11) NOT NULL,
  Product text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE category (
  id int(11) NOT NULL,
  title varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO category (id, title) VALUES
(1, 'Grocery'),
(2, 'Dairy'),
(3, 'Liquor'),
(4, 'Canned Food'),
(5, 'Soft Drinks'),
(6, 'Ready to Eat');


CREATE TABLE products (
  ID int(11) NOT NULL,
  imgUrl text NOT NULL,
  Product text NOT NULL,
  Description text NOT NULL,
  Price double NOT NULL,
  Category text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO products (ID, imgUrl, Product, Description, Price, Category) VALUES
(2, '4.jpg', 'Broccoli', 'Fresh Broccoli', 1.2, 'Grocery'),
(3, '2.jpg', 'Canned Corn', 'Dellote Canned Corn', 10.5, 'Canned Food'),
(4, '6.jpg', 'Corana\r\n', '6 Packs of Corona 0.8ml.', 9.99, 'Liquor'),
(6, '8.jpg', 'Soft Drinks', 'Can Sprite 320ml.', 2.5, 'Soft Drinks'),
(7, '10.jpg', 'BreakFast', '3 Minute BreakFast', 2.2, 'Ready to Eat'),
(8, '9.jpg', 'Ready Food', 'Chicken Curry plus Rice', 4.2, 'Ready to Eat'),
(11, '7.jpg', 'Soft Drinks', 'Can Coke 320ml.', 2.5, 'Soft Drinks'),
(80, '3.jpg', 'Tomato', 'Fresh Tomatoes', 1.5, 'Grocery'),
(81, '5.png', 'Hennessy\r\n', 'Hennessy 450ml.', 49.99, 'Liquor'),
(82, '1.png', 'Canned Beans', 'Black Beans', 12.5, 'Canned Food'),
(123, '11.jpg', 'Milk', 'Fresh Milk 1.2L', 6.99, 'Dairy'),
(245, '12.jpg', 'Yogurt', 'Natural Yogurt 78g.', 8.99, 'Dairy');


CREATE TABLE users (
  id int(10) NOT NULL,
  fname text NOT NULL,
  lname text NOT NULL,
  phone int(10) NOT NULL,
  email text NOT NULL,
  username varchar(32) NOT NULL,
  password varchar(32) NOT NULL,
  hash varchar(32) NOT NULL,
  active int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO users (id, fname, lname, phone, email, username, password, hash, active) VALUES
(1, 'sam ', 'jones', 25235325, 'shss@ahah.com', 'samjones', 'md5(work)', '2a084e55c87b1ebcdaad1f62fdbbac8e', 0),
(14, 'genius', 'macnus', 3628463, 'hsahh@633.cn', 'webapp', 'md5(qwerty)', '6c8349cc7260ae62e3b1396831a8398f', 0),
(17, 'james', 'bond', 252636, 'james@bond.com', '007', 'bond', '47d1e990583c9c67424d369f3414728e', 0),
(25, 'wonder ', 'woman', 253737, 'wonder@woman.com', 'wonder', 'woman', 'd09bf41544a3365a46c9077ebb5e35c3', 0);


CREATE TABLE `order` (
  id int(11) NOT NULL,
  name varchar(32) NOT NULL,
  contact int(10) NOT NULL,
  address varchar(32) NOT NULL,
  email varchar(32) NOT NULL,
  item text NOT NULL,
  amount varchar(32) NOT NULL,
  status varchar(32) NOT NULL,
  dateOrdered varchar(32) NOT NULL,
  dateDelivered varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `order` (id, name, contact, address, email, item, amount, status, dateOrdered, dateDelivered) VALUES
(9, 'john hohn', '234', 'dsdfdg', 'a@yahoo.com', '(1) Product 202, ', '100', 'confirmed', '06/14/15 09:53:36 AM', '06/14/15 09:53:56 AM'),
(10, 'a a', 'e424', 'dsdfdg', 'a@yahoo.com', '(1) Product 202, (2) Product 101, ', '400.5', 'delivered', '06/14/15 10:02:11 AM', '06/14/15 10:02:52 AM'),
(11, 'sam jones', '2536373731', 'dhdyeheh', 'aga@ya.cn', '', '0', 'delivered', '09/11/18 11:20:08 AM', '09/11/18 12:04:37 PM'),
(13, 'well done', '123456', '283 city road', 'abc@abc.cn', '(1) Tomato, (1) Broccoli, ', '0', 'unconfirmed', '09/11/18 03:17:43 PM', '');

ALTER TABLE cart
  ADD PRIMARY KEY (ID);

ALTER TABLE category
  ADD PRIMARY KEY (id);

ALTER TABLE `order`
  ADD PRIMARY KEY (id);

ALTER TABLE products
  ADD PRIMARY KEY (ID);

ALTER TABLE users
  ADD PRIMARY KEY (id);


ALTER TABLE cart
  MODIFY ID int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE category
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `order`
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;


ALTER TABLE products
  MODIFY ID int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

ALTER TABLE users
  MODIFY id int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
