// jQuery(function($) {'use strict',
//
// 	var form = $('.contact-form');
// 	form.submit(function () {'use strict',
// 		$this = $(this);
// 		$.post("sendemail.php", $(".contact-form").serialize(),function(result){
// 			if(result.type == 'success'){
// 				$this.prev().text(result.message).fadeIn().delay(3000).fadeOut();
// 			}
// 		});
// 		return false;
// 	});
//
// });

// Google Map Customization
// (function(){
//
// 	var map;
//
// 	map = new GMaps({
// 		el: '#gmap',
// 		lat: 43.1580159,
// 		lng: -77.6030777,
// 		scrollwheel:false,
// 		zoom: 14,
// 		zoomControl : false,
// 		panControl : false,
// 		streetViewControl : false,
// 		mapTypeControl: false,
// 		overviewMapControl: false,
// 		clickable: false
// 	});
//
// 	var image = 'images/map-icon.png';
// 	map.addMarker({
// 		lat: 43.1580159,
// 		lng: -77.6030777,
// 		// icon: image,
// 		animation: google.maps.Animation.DROP,
// 		verticalAlign: 'bottom',
// 		horizontalAlign: 'center',
// 		backgroundColor: '#ffffff',
// 	});
//
// 	var styles = [
//
// 	{
// 		"featureType": "road",
// 		"stylers": [
// 		{ "color": "" }
// 		]
// 	},{
// 		"featureType": "water",
// 		"stylers": [
// 		{ "color": "#A2DAF2" }
// 		]
// 	},{
// 		"featureType": "landscape",
// 		"stylers": [
// 		{ "color": "#ABCE83" }
// 		]
// 	},{
// 		"elementType": "labels.text.fill",
// 		"stylers": [
// 		{ "color": "#000000" }
// 		]
// 	},{
// 		"featureType": "poi",
// 		"stylers": [
// 		{ "color": "#2ECC71" }
// 		]
// 	},{
// 		"elementType": "labels.text",
// 		"stylers": [
// 		{ "saturation": 1 },
// 		{ "weight": 0.1 },
// 		{ "color": "#111111" }
// 		]
// 	}
//
// 	];
//
// 	map.addStyle({
// 		styledMapName:"Styled Map",
// 		styles: styles,
// 		mapTypeId: "map_style"
// 	});
//
// 	map.setStyle("map_style");
// }());


var config = {
	apiKey: "AIzaSyAX_SNl8nYYA49IoP7kBA176mnFUKpLnDw",
	authDomain: "cloud-pro-1.firebaseapp.com",
	databaseURL: "https://cloud-pro-1.firebaseio.com",
	projectId: "cloud-pro-1",
	storageBucket: "cloud-pro-1.appspot.com",
	messagingSenderId: "264066071121"
};
firebase.initializeApp(config);


// Reference messages collection
var messagesRef = firebase.database().ref('messages');

// Listen for form submit
document.getElementById('main-contact-form').addEventListener('submit', submitForm);

// Submit form
function submitForm(e){
  e.preventDefault();

  // Get values
  var name = getInputVal('name');
  var email = getInputVal('email');
  var phone = getInputVal('phone');
	var subject = getInputVal('subject');
  var message = getInputVal('message');

  // Save message
  saveMessage(name, email, phone, subject, message);

  // Show alert
  document.querySelector('.alert-success').style.display = 'block';

  // Hide alert after 3 seconds
  setTimeout(function(){
    document.querySelector('.alert-success').style.display = 'none';
  },3000);

  // Clear form
  document.getElementById('main-contact-form').reset();
}

// Function to get get form values
function getInputVal(id){
  return document.getElementById(id).value;
}

// Save message to firebase
function saveMessage(name, email, phone, subject, message){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    name: name,
    email:email,
    phone:phone,
		subject:subject,
    message:message
  });
}
