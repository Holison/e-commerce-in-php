firebase.auth().onAuthStateChanged(function(user) {

  if (user) {
    // User is signed in.
    photoUrl = user.photoURL;
    uid = user.uid;

    document.getElementById("in").style.display = "none";
    document.getElementById("out").style.display = "block";
    //document.getElementById('user').innerHTML = user.displayName;
    //document.getElementById('out').innerHTML = '<a href="logout.php" >Logout</a>';
    var user = firebase.auth().currentUser;
    var name, email, photoUrl, uid, emailVerified;
    if(user != null){

      name = user.displayName;
      document.getElementById("user").innerHTML = "Welcome : " + name;

    }

  } else {
    // No user is signed in.
    document.getElementById("in").style.display = "block";
    document.getElementById("out").style.display = "none";

  }
});


function login(){

  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;

  firebase.auth().signInWithEmailAndPassword(username, password).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

    // ...
  });

}



function logout(){
  firebase.auth().signOut();
}


(function(){

var ui = new firebaseui.auth.AuthUI(firebase.auth());

var uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
      // User successfully signed in.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      return true;
    },
    uiShown: function() {
      // The widget is rendered.
      // Hide the loader.
      document.getElementById('loader').style.display = 'none';
    }
  },
  // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
  signInFlow: 'popup',
  signInSuccessUrl: 'index.php',
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    //firebase.auth.GithubAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
    //firebase.auth.PhoneAuthProvider.PROVIDER_ID
  ],
  // Terms of service url.
  tosUrl: '#',
  // Privacy policy url.
  privacyPolicyUrl: '#'
};
ui.start('#firebaseui-auth-container', uiConfig);

})()
