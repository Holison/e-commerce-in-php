firebase.auth().onAuthStateChanged(function(user) {

  if (user) {
    // User is signed in.
    document.getElementById("checkout").submit();

  } else {
    // No user is signed in.
    window.alert("Please login to checkout" );
    window.location.href = '/login.php';
  }
});
